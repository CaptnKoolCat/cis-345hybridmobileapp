import {Image, Text, View} from "react-native";
import React from "react";
import Colors from "../constants/Colors";

export function ForecastListItem({timeSlot}){
    const container = {
        flexDirection: 'row',
        paddingTop: 4,
        paddingBottom: 4,
    };
    const text = {
        flex: 1,
        paddingLeft: 4,
        textAlignVertical: 'center'
    };
    const time = {
        paddingRight: 10,
        textAlignVertical: 'center',
        alignSelf: 'flex-end'
    };
    const icon = {
      height: 50,
      width: 50,
    };

    const weatherIconUrl = {uri: "https://openweathermap.org/img/w/" + timeSlot.weatherIcon + ".png"};

    return(
        <View style={container}>
            <Image source={weatherIconUrl} style={icon}/>
            <Text style={text}>{timeSlot.currentTemp}°F</Text>
            <Text style={time}>{timeSlot.time}</Text>
        </View>
    );
}
