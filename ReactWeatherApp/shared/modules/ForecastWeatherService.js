import {ForecastTimeslot} from "../ForecastTimeslot";

const DefaultUrl = 'http://api.openweathermap.org/data/2.5/forecast';
const ApiKey = '&appid=deed45c2fb259de522aaa12665b7bfe4';

export class ForecastWeatherService {
    #data;

    constructor(url) {
        this.baseUrl = url || DefaultUrl;
    }

    forecastReturn(url){
        return(fetch(url).then((res) => res.json())
            .then((data) => {
                let timeSlots = (data.list).map((item) => {
                    return(ForecastTimeslot.fromJson(item));
                });
                this.#data = timeSlots;
                return(timeSlots)
            }).catch((err) => {
                console.log('No forecast retrieved: ERROR: ' + err);
            }));
    }

    capitalize(str){
        return str.charAt(0).toUpperCase() + str.slice(1);
    }

    forecastByCoords(lat, lon){
        const url = this.baseUrl + "?lat=" + lat + "&lon=" + lon + ApiKey;
        console.log("Get forecast with coords: " + lat + " " + lon);

        return(this.forecastReturn(url))
    }

    forecastByZip(zip){
        const url = this.baseUrl + '?zip=' + zip + ApiKey;
        return(this.forecastReturn(url));
    }

    forecastByCityAndState(city, state){
        if(!state){
            state = "";
        } else {
            state = "," + state;
        }
        const url = this.baseUrl + '?q=' + city + ',' + state + ApiKey;

        return(this.forecastReturn(url));
    }

}
