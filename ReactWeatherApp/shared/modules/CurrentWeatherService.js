

const DefaultUrl = 'http://api.openweathermap.org/data/2.5/weather';
const ApiKey = '&appid=deed45c2fb259de522aaa12665b7bfe4';

export class CurrentWeatherService{
    #data;

    constructor(url) {
        this.baseUrl = url || DefaultUrl;
    }

    weatherReturn(url){
        return(fetch(url).then((res) => res.json())
            .then((data) => {
                let weather = data;
                this.#data = weather;
                return(weather);
            }).catch((err) => {
                console.log('No weather retrieved: ERROR: ' + err);
            }));
    }

    convertToFahrenheit(temp){
        return(((temp - 273.15) * (9/5) + 32).toFixed(0))
    }

    convertToMilesPerHour(speed){
        return((speed * 2.2369).toFixed(2))
    }

    capitalize(str){
        return str.charAt(0).toUpperCase() + str.slice(1);
    }

    getWeatherByZip(zip){
        const url = this.baseUrl + '?zip=' + zip + ApiKey;
        return(this.weatherReturn(url));

    }

    getWeatherByCoords(lat, long){
        const url = this.baseUrl + '?lat=' + lat + '&lon=' + long + ApiKey;
        return(this.weatherReturn(url))
    }

    getWeatherByCityAndState(city, state){
        if(!state){
            state = "";
        } else {
            state = "," + state;
        }
        const url = this.baseUrl + '?q=' + city + ',' + state + ApiKey;

        return(this.weatherReturn(url))
    }

}
