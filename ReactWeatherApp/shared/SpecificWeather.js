export class SpecificWeather{
    zipCode;
    lat;
    lon;
    city;
    stateLocation;

    constructor(properties = {
        zipCode: null, lat: null, lon: null, city: null,
    }) {
        Object.assign(this, properties);
    }
}
