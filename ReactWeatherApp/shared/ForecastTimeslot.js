export class ForecastTimeslot {
    id;
    currentTemp;
    highTemp;
    lowTemp;
    weatherDescription;
    weatherIcon;
    windSpeed;
    rain;
    snow;
    time;

    constructor(properties = {id: null, currentTemp: null, highTemp: null, lowTemp: null,
                             weatherDescription: null, weatherIcon: null, windSpeed: null, rain: null, snow: null, time: null}) {
        Object.assign(this, properties)
    }

    static convertToFahrenheit(temp){
        return(((temp - 273.15) * (9/5) + 32).toFixed(0))
    }

    static convertToMilesPerHour(speed){
        return((speed * 2.2369).toFixed(2))
    }

    static fromJson(json){
        if(json == null){
            return(null);
        }
        let jsonObj = Object.assign(Object.create(ForecastTimeslot.prototype), {
            id: json.dt,
            currentTemp: this.convertToFahrenheit(json.main.temp),
            highTemp: this.convertToFahrenheit(json.main.temp_max),
            lowTemp: this.convertToFahrenheit(json.main.temp_min),
            weatherDescription: json.weather[0].description,
            weatherIcon: json.weather[0].icon,
            windSpeed: this.convertToMilesPerHour(json.wind.speed),
            time: json.dt_txt,
        });

        if("rain" in json){
            jsonObj["rain"] = "Rain Levels: " +json.rain["3h"]
        }

        if("snow" in json){
            jsonObj["snow"] = "Snow Levels: " + json.snow["3h"]
        }

        return (jsonObj);
    }
}
