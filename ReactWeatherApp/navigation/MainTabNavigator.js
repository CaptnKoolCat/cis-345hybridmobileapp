import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import ForecastScreen from '../screens/ForecastScreen';
import SettingsScreen from '../screens/SettingsScreen';
import SpecificWeatherScreen from '../screens/SpecificWeatherScreen'
import ForecastDetailScreen from "../screens/ForecastDetailScreen";

const config = Platform.select({
  web: { headerMode: 'screen' },
  default: {},
});

const HomeStack = createStackNavigator(
  {
    Home: HomeScreen,
      SpecificWeather: SpecificWeatherScreen,
  },
  config
);

HomeStack.navigationOptions = {
  tabBarLabel: 'Local Weather',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? 'ios-rainy'
          : 'md-rainy'
      }
    />
  ),
};

HomeStack.path = '';

const ForecastStack = createStackNavigator(
  {
    Forecast: ForecastScreen,
      ForecastDetail: ForecastDetailScreen,
  },
  config
);

ForecastStack.navigationOptions = {
  tabBarLabel: '5-Day Forecast',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} name={Platform.OS === 'ios' ? 'ios-link' : 'md-link'} />
  ),
};

ForecastStack.path = '';

const SettingsStack = createStackNavigator(
  {
    Settings: SettingsScreen,
  },
  config
);

SettingsStack.navigationOptions = {
  tabBarLabel: 'Settings',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} name={Platform.OS === 'ios' ? 'ios-options' : 'md-options'} />
  ),
};

SettingsStack.path = '';

const tabNavigator = createBottomTabNavigator({
  HomeStack,
  ForecastStack,
  SettingsStack,
});

tabNavigator.path = '';

export default tabNavigator;
