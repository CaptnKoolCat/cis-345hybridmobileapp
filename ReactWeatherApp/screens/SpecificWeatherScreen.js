import React from 'react';
import {Text, View, StyleSheet, TextInput, Button, TouchableOpacity, Image} from "react-native";
import {CurrentWeatherService} from "../shared/modules/CurrentWeatherService";
import Colors from "../constants/Colors";
import {SpecificWeather} from "../shared/SpecificWeather";


function InputView(props) {
    const defaultStyle = StyleSheet.create({
        container: {
            paddingTop: 15,
            paddingLeft: 25,
            paddingRight: 25,
            alignSelf: 'stretch',
        },
        label: {
            fontSize: 20,
        },
    });

    const {label} = props;
    return (
        <View style={[defaultStyle.container]}>
            <Text style={defaultStyle.label}>{label}</Text>
            {props.children}
        </View>
    );
}

function WeatherView(props) {
    const {lowTemp, highTemp, weatherIconUrl, currentTemp, windSpeeds} = props;

    return(
    <View style={styles.container}>
                <View style={{flexDirection: 'row', alignSelf: 'center'}}>
                        <Text> {currentTemp}  {weatherIconUrl}</Text>
                </View>

                <View style={{flexDirection: 'row', alignSelf: 'center'}}>
                    <Text style={styles.minMaxTemp}>  {lowTemp}  {highTemp}  </Text>
                    <Text>{windSpeeds}</Text>
                </View>
    </View>
    )

}

export default class SpecificWeatherScreen extends React.Component{
    static navigationOptions = {
        title: 'Specific Weather'
    };

    #currentWeatherService;

    constructor(props) {
        super(props);
        this.#currentWeatherService = new CurrentWeatherService();
        this.state = {
            hasLocationPermission: null,
        }
    }

    specificSetState(weather){
        this.setState({
            currentTemp: "Current Temp: " + (this.#currentWeatherService.convertToFahrenheit(weather.main.temp)).toString() + "°F",
            highTemp: "High Temp: " + (this.#currentWeatherService.convertToFahrenheit(weather.main.temp_max)).toString() + "°F",
            lowTemp: "Low Temp: " + (this.#currentWeatherService.convertToFahrenheit(weather.main.temp_min)).toString() + "°F" ,
            windSpeeds: "Wind Speeds: " + (this.#currentWeatherService.convertToMilesPerHour(weather.wind.speed)).toString() + " mph",
            weatherDesc: "Weather Description: " + this.#currentWeatherService.capitalize(weather.weather[0].description) + ".",
        });
    }

    handleZipPress(zip) {
        this.#currentWeatherService.getWeatherByZip(zip).then((weather) => {
            this.specificSetState(weather);
        });
    }

    handleCityStatePress(city, state){
        this.#currentWeatherService.getWeatherByCityAndState(city, state).then((weather) => {
            this.specificSetState(weather);

        });


    }

    handleCoordPress(lat, lon){
        this.#currentWeatherService.getWeatherByCoords(lat, lon).then((weather) => {
            this.specificSetState(weather);

        });
    }

    render(){
        const {lowTemp, highTemp, weatherDesc, currentTemp, windSpeeds} = this.state;

        const buttonCity = (this.state.city) ? <Button style={styles.button} onPress={() => this.handleCityStatePress(this.state.city, this.state.stateLocation)} title="Search City Weather"/>
                                            : <Text/>;
        const coordButton = (this.state.lat && this.state.lon) ? <Button style={styles.button} onPress={() => this.handleCoordPress(this.state.lat, this.state.lon)} title="Search Coordinates Weather"/>
                                            : <Text/>;
        return(
            <View>

                <InputView label={"Search weather by zip code."}>
                    <TextInput style={[styles.input, styles.inputUnderline]}
                               placeholder={"Enter a zip code.."} onSubmitEditing={(text) => this.handleZipPress(text.nativeEvent.text)}/>
                </InputView>

                <InputView label={"Search Weather by City and/or State"}>
                    <TextInput style={[styles.input, styles.inputUnderline]} onSubmitEditing={(text) => this.setState({city: text.nativeEvent.text})} placeholder={"Enter a 'City'.."}/>
                    <TextInput style={[styles.input, styles.inputUnderline]} onSubmitEditing={(text) => this.setState({stateLocation: text.nativeEvent.text})} placeholder={"Optional: Enter a 'State'.. Ex - 'us 'au' 'uk'"}/>
                </InputView>
                <View>
                    {buttonCity}
                </View>
                <InputView label={"Search weather by Latitude and Longitude"}>
                    <TextInput style={[styles.input, styles.inputUnderline]} onSubmitEditing={(text) => this.setState({lat: text.nativeEvent.text})} placeholder={"Enter a Latitude.."}/>
                    <TextInput style={[styles.input, styles.inputUnderline]} onSubmitEditing={(text) => this.setState({lon: text.nativeEvent.text})} placeholder={"Enter a Longitude.."}/>
                </InputView>
                <View>
                    {coordButton}
                </View>
                
                <View style={{paddingTop: 20}}>
                    <WeatherView lowTemp={lowTemp} highTemp={highTemp} weatherIconUrl={weatherDesc} currentTemp={currentTemp}
                                     windSpeeds={windSpeeds}/>
                </View>
            </View>
        );

    }
}

const styles = StyleSheet.create({

    button: {
        marginLeft: 50,
        marginRight: 50,
    },
    input: {
        color: Colors.tintColor
    },
    inputUnderline: {
        borderColor: Colors.tabIconDefault,
        borderBottomWidth: 1
    },
});
