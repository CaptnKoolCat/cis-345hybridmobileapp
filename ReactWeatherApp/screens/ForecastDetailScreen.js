import {Image, Text, TouchableOpacity, View, StyleSheet} from "react-native";
import React from "react";


export default class ForecastDetailScreen extends React.Component {
    constructor(props) {
        super(props);
        const timesSlot = this.props.navigation.state.params;
        if(!timesSlot){
            console.log("No forecast timeslot found. Returning to 5-day forecast");
            this.props.navigation.goBack();
            return;
        }

        this.props.navigation.setParams({screenTitle: "Forecast Detail"});

        this.state = {
            timesSlot: timesSlot,
        }
    }

    render(){
        const timeSlot = this.state.timesSlot;
        const weatherIconUrl = {uri: "https://openweathermap.org/img/w/" + timeSlot.timeSlot.weatherIcon + ".png"};

        if(timeSlot.timeSlot)

        return(
            <View style={styles.container}>
                <View style = {styles.currentLocationHeaderContainer}>
                    <Text style = {styles.currentLocationHeaderText}>Local Weather</Text>
                </View>
                <View style = {styles.contentContainer}>

                    <View style = {styles.currentTempContainer}>
                        <Text style = {styles.currentTempText}> {timeSlot.timeSlot.currentTemp}°F</Text>
                        <View style={{flexDirection: 'row'}}>
                            <Image source={weatherIconUrl} style={styles.weatherIcon}/>

                        </View>
                    </View>

                    <View style={styles.otherTempsContainer}>
                        <View>
                            <Text style={styles.minMaxTemp}>{timeSlot.timeSlot.lowTemp}°F  {timeSlot.timeSlot.highTemp}°F</Text>
                        </View>
                        <View style={{flexDirection: 'row', flex: 2, paddingTop: 50}}>

                            <View>
                                <Text>{timeSlot.timeSlot.rain}  {timeSlot.timeSlot.snow}</Text>
                            </View>

                        </View>
                        <View style={styles.windContainer}>
                            <Text>Wind Speeds: {timeSlot.timeSlot.windSpeed} mph   </Text>
                        </View>

                    </View>

                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
    },
    contentContainer: {
        paddingTop: 30,
    },
    currentLocationHeaderContainer: {
        paddingTop: 40,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    currentLocationHeaderText: {
        fontSize: 24,
        fontWeight: 'bold',
    },
    currentTempContainer: {
        paddingTop: 40,
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center',
    },
    currentTempText: {
        fontSize: 36,
        fontWeight: 'bold',
    },
    otherTempsContainer: {
        paddingTop: 100,
        flex: 3,
        justifyContent: 'center',
        alignItems: 'center',
    },
    minMaxTemp: {
        fontSize: 24,
        fontWeight: 'bold',
    },
    weatherIcon: {
        height: 50,
        width: 50,
    },
    windContainer: {
        paddingTop: 20,
    },
});
