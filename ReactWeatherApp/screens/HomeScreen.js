import * as WebBrowser from 'expo-web-browser';
import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';
import {CurrentWeatherService} from "../shared/modules/CurrentWeatherService";

export default class HomeScreen extends React.Component{

  static navigationOptions = {
    title: 'Local Weather'
  };

  currentWeatherService;

  constructor(props) {
    super(props);
    this.currentWeatherService = new CurrentWeatherService();
    this.state = {
      hasLocationPermission: null,
    }
  }

  componentDidMount() {
    Permissions.askAsync(Permissions.LOCATION).then((response) => {
      if(response.granted){
        const locationOptions = {enableHighAccuracy: true};
        Location.getCurrentPositionAsync(locationOptions).then((location) => {
          this.setState({
            location: location.coords,
            longitude: location.coords.longitude,
            latitude: location.coords.latitude,
            hasLocationPermission: true
          });
        }).finally(() => {
          this.currentWeatherService.getWeatherByCoords(this.state.latitude, this.state.longitude)
              .then((weather) => {
                const weatherIconUrl = "https://openweathermap.org/img/w/" + weather.weather[0].icon + ".png";
                this.setState({
                  currentTemp: this.currentWeatherService.convertToFahrenheit(weather.main.temp),
                  highTemp: this.currentWeatherService.convertToFahrenheit(weather.main.temp_max),
                  lowTemp: this.currentWeatherService.convertToFahrenheit(weather.main.temp_min),
                  windSpeeds: this.currentWeatherService.convertToMilesPerHour(weather.wind.speed),
                  weatherIcon: weatherIconUrl,
                  weatherDescription: this.currentWeatherService.capitalize(weather.weather[0].description),
                  countyName: weather.name + " County",
                });
                if('snow' in weather){
                  this.setState({
                    snow: "Snow Levels: " + weather.snow["3h"] + " mm" || "Snow Levels: " + weather.snow["1h"] + " mm" || "",
                  })
                }
                if('rain' in weather){
                  this.setState({
                    rain: "Rain Levels: " + weather.rain["3h"] + " mm" || "Rain Levels: " + weather.rain["1h"] + " mm" || "",
                  })
                }
              })
        });
      }
    })
  }

  handleWeatherSomewhereElse() {
    const {navigate} = this.props.navigation;
    navigate('SpecificWeather');
  }

  render() {
    const highTemp = this.state.highTemp;
    const lowTemp = this.state.lowTemp;
    const currentTemp = this.state.currentTemp;
    const windSpeeds = this.state.windSpeeds;
    const rain = this.state.rain;
    const snow = this.state.snow;
    const weatherIconUrl = { uri: this.state.weatherIcon};
    const countyName = this.state.countyName;

    return (
        <View style={styles.container}>
          <View style = {styles.currentLocationHeaderContainer}>
            <Text style = {styles.currentLocationHeaderText}>{countyName} - Local Weather</Text>
          </View>
          <View style = {styles.contentContainer}>

            <View style = {styles.currentTempContainer}>
              <Text style = {styles.currentTempText}> {currentTemp}°F</Text>
              <View style={{flexDirection: 'row'}}>
                <Image source={weatherIconUrl} style={styles.weatherIcon}/>

              </View>
            </View>

            <View style={styles.otherTempsContainer}>
              <View>
                <Text style={styles.minMaxTemp}>{lowTemp}°F  {highTemp}°F</Text>
              </View>
              <View style={{flexDirection: 'row', flex: 2, paddingTop: 50}}>

                <View>
                  <Text>{rain}  {snow}</Text>
                </View>

              </View>
              <View style={styles.windContainer}>
                <Text>Wind Speeds: {windSpeeds} mph   </Text>
              </View>

            </View>

          </View>
          <TouchableOpacity style={styles.buttonContainer} onPress={this.handleWeatherSomewhereElse.bind(this)}>
            <Text style={styles.buttonText}>Other Location's Weather</Text>
          </TouchableOpacity>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
  },
  contentContainer: {
    paddingTop: 30,
  },
  currentLocationHeaderContainer: {
    paddingTop: 40,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  currentLocationHeaderText: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  currentTempContainer: {
    paddingTop: 40,
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  currentTempText: {
    fontSize: 36,
    fontWeight: 'bold',
  },
  otherTempsContainer: {
    paddingTop: 100,
    flex: 3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  minMaxTemp: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  weatherIcon: {
    height: 50,
    width: 50,
  },
  windContainer: {
    paddingTop: 20,
  },
  buttonContainer: {
    backgroundColor: "blue",
    borderColor: "blue",
    borderWidth: 1,
    borderRadius: 10,
    margin: 100,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'white',
    padding: 5,

  }

});
