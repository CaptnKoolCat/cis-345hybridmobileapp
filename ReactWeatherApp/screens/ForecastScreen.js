import React from 'react';
import { ScrollView, StyleSheet, FlatList, View, Text, TouchableHighlight } from 'react-native';
import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';
import {ForecastWeatherService} from "../shared/modules/ForecastWeatherService";
import {ForecastListItem} from "../components/ForecastListItem";
import Colors from "../constants/Colors";

export default class ForecastScreen extends React.Component {

  static navigationOptions = {
    title: '5-Day Forecast',
  };

  #forecastService;

  constructor(props) {
    super(props);
    this.#forecastService = new ForecastWeatherService();
    this.state ={
      timeSlots: props.timeSlots || [],
    }
  }

  componentDidMount() {
    Permissions.askAsync(Permissions.LOCATION).then((response) => {
      if(response.granted){
        const locationOptions = {enableHighAccuracy: true};
        Location.getCurrentPositionAsync(locationOptions).then((location) => {
          this.setState({
            location: location.coords,
            longitude: location.coords.longitude,
            latitude: location.coords.latitude,
            hasLocationPermission: true
          });
        }).finally(() => {
          this.#forecastService.forecastByCoords(this.state.latitude, this.state.longitude)
              .then((timeSlots) => {
                this.setState({
                  timeSlots: timeSlots,
                })
              }).catch((err) => {
                this.setState({
                  timeSlots: [],
                  error
                })
          })
        });
      }
    })
  }

  handlePress(timeSlot){
    const {navigate} = this.props.navigation;
    navigate('ForecastDetail', {timeSlot: timeSlot})
  }

  render() {
    const timesSlots = this.state.timeSlots || [];

    return (
        <FlatList data={timesSlots} renderItem={({item, index, separators}) => {
      return (
        <TouchableHighlight onPress={() => this.handlePress(item)}
                            underlayColor={Colors.highlight}
                            onShowUnderlay={separators.highlight}
                            onHideUnderlay={separators.unhighlight}>
          <ForecastListItem timeSlot={item}/>
        </TouchableHighlight>

      );
    }}
    keyExtractor={item => item.id.toString()}
    ListEmptyComponent={<View><Text>No Forecast available.</Text></View>}
    ItemSeparatorComponent={() => <View style={styles.line}/>}
    />
    );
  }

}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },

  line: {
    height: 0.5,
    width: '100%',
    backgroundColor: "rgba(200,200,200,0.5)"
  },
});
