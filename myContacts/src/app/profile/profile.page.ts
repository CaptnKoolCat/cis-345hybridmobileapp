import {Component, OnDestroy, OnInit} from '@angular/core';
import {Settings} from '../shared/models/settings';
import {ProfileService} from '../core/profile.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit, OnDestroy {
  readonly DefaultPhoto: string = './assets/camera.png';
  private settings: Settings;
  private subscription: Subscription;

  constructor(private profileService: ProfileService) {
    this.settings = this.getDefaultSettings();
    this.subscription = new Subscription();
  }

  ngOnInit() {
    this.subscription.add(
      this.profileService.get().subscribe((settings) => {
        this.settings = settings || this.getDefaultSettings();
      })
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  handleSave(event: any) {
    this.profileService.save(this.settings).then((settings) => {
      this.settings = settings || this.getDefaultSettings();
    }).catch((error) => {
      console.log(error);
    });
  }

  getDefaultSettings() {
    return(new Settings( {
      photo: this.DefaultPhoto,
    }));
  }

  handlePhoto(event: MouseEvent) {
    console.log(this.settings);
    if (this.profileService.isCameraAvailable()) {
      this.profileService.takePhoto().then((photo) => {
        // console.log(photo);
        this.settings.photo = 'data:image/jpeg;base64,' + photo;
      });
    } else {
      console.log('camera not available');
    }
  }
}
