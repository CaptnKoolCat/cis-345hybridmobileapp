import { Address } from './address';

describe('Address', () => {
  it('should create an instance', () => {
    expect(new Address()).toBeTruthy();
  });

  describe('fromJson', () => {
    const json = {
      street: {
        number: 9377,
        name: 'W Sherman Dr'
      },
      city: 'Mckinney',
      state: 'Washington',
      country: 'United States',
      postcode: 68275,
      coordinates: {
        latitude: '-72.0977',
        longitude: '54.1531'
      },
      timezone: {
        offset: '-11:00',
        description: 'Midway Island, Samoa'
      }
    };

    it ('with null json returns null', () => {
      const address: Address = Address.fromJson(null);
      expect(address).toBeFalsy();
    });

    it ('should assign all properties', () => {
      const  address: Address = Address.fromJson(json);
      expect(address.street).toEqual('9377 W Sherman Dr');
      expect(address.city).toEqual('Mckinney');
      expect(address.state).toEqual('Washington');
      expect(address.zipcode).toEqual('68275');
      expect(address.country).toEqual('United States');
      expect(address.latitude).toEqual(-72.0977);
      expect(address.longitude).toEqual(54.1531);
    });
  });

  describe('distanceFrom', () => {
    // tslint:disable:variable-name
    const stevenspoint_latitude = 44.5236;
    const stevenspoint_longitude = 89.5746;
    const madison_latitude = 43.0731;
    const madison_longitude = 89.4012;
    // tslint:enable:variable-name

    it('should return 100.59 from stevens point to madison', () => {
      // Arrange
      const address = new Address();
      address.latitude = stevenspoint_latitude;
      address.longitude = stevenspoint_longitude;

      const expected = 100.59;
      // Act
      const distance = address.distanceFrom(madison_latitude, madison_longitude);

      // Assert
      expect(distance).toBeCloseTo(expected, 1);
    });
  });
});
