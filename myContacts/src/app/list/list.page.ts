import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserService} from '../core/user.service';
import {Observable, Subscription } from 'rxjs';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {User} from '../shared/models/user';
import {NavController} from '@ionic/angular';
import {ProfileService} from '../core/profile.service';
import {Settings} from '../shared/models/settings';
import {subscriptionLogsToBeFn} from 'rxjs/internal/testing/TestScheduler';

@Component({
  selector: 'app-list',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss']
})
export class ListPage implements OnInit, OnDestroy {
  readonly DefaultContactLimit: number = 10;
  readonly MaximumContactLimit: number = 2500;
  private users: User[];
  private range = '0';
  private subscription: Subscription;
  private dataSubscription: Subscription;
  private  settings: Settings;
  private  location: Coordinates;

  constructor(private userService: UserService,
              private  profileService: ProfileService,
              private  geolocation: Geolocation,
              private navController: NavController) {
    this.subscription = new Subscription();
  }

  ngOnInit() {
    this.subscription.add(
        this.profileService.get().subscribe((settings) => {
          this.settings = settings || new Settings();
          this.refresh(this.settings.contactLimit);
        })
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  applyRangeFilter(users: Array<User>, limit?: number, location?: Coordinates, range?: number) {
    range = range || parseInt(this.range, 10);
    location = location || this.location;
    limit = limit || this.settings.contactLimit || this.DefaultContactLimit;

    if (location && range) {
      users = users.filter((user) => {
        console.log(location);
        return(user.address.distanceFrom(location.latitude, location.longitude) <= range);
      });
    }
    return(users.slice(0, limit));
  }

  refresh(limit?: number) {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.location = resp.coords;
    }).finally(() => {
      this.subscription.remove(this.dataSubscription);
      // if limiting to a range, grab the first 2500 results
      // We'll have to filter out data on the client side.
      const range: number = parseInt(this.range, 10);
      const adjustedLimit: number = (! range) ? (limit || this.settings.contactLimit || this.DefaultContactLimit)
                                    : this.MaximumContactLimit;
      this.dataSubscription = this.userService.all(adjustedLimit).subscribe((users) => {
        this.users = this.applyRangeFilter(users, limit, this.location, range);
      });

      this.subscription.add(this.dataSubscription);
    });
  }

  displayUserDetail(user: User) {
    this.navController.navigateForward(['/detail/' + user.id], {
      state: user
    });
  }

    handlerFilter($event: Event) {
      this.refresh();
    }
}
