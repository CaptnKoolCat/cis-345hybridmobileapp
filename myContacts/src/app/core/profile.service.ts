import { Injectable } from '@angular/core';
import {Camera, CameraOptions} from '@ionic-native/camera/ngx';
import {Storage} from '@ionic/storage';
import {from, Observable} from 'rxjs';
import {Settings} from '../shared/models/settings';
import {Platform} from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  readonly ProfileSettingName: string = 'profile';
  readonly  DefaultCameraSettings: CameraOptions = {
    quality: 20,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
  };
  constructor(private platform: Platform, private storage: Storage, private camera: Camera) {

  }

  get(): Observable<Settings> {
    return(from(this.storage.get(this.ProfileSettingName)));
  }

  save(settings: Settings): Promise<any> {
    return(this.storage.set(this.ProfileSettingName, settings));
  }

  takePhoto(options?: CameraOptions) {
    options = options || this.DefaultCameraSettings;
    return(this.camera.getPicture(options));
  }

  isCameraAvailable(): boolean {
    console.log(this.platform.platforms());
    return (this.platform.is('android') ||
            this.platform.is('ios'));
  }

}
