import { TestBed } from '@angular/core/testing';

import { UserService } from './user.service';
import {HttpClient, HttpClientModule} from '@angular/common/http';

describe('UserService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [ HttpClientModule ]
  }));

  it('should be created', () => {
    const service: UserService = TestBed.get(UserService);
    expect(service).toBeTruthy();
  });

  describe('all', () => {
    it ('should return more than one user', (done) => {
      const service: UserService = TestBed.get(UserService);
      service.all().subscribe((users) => {
        expect(users).toBeTruthy();
        expect(users.length).toBeGreaterThanOrEqual(1);
        done();
      });
    });
  });
});
