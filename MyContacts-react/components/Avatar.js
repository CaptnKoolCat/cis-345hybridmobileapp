import {Image} from "react-native";
import React from "react";

export function Avatar({user, size=60, backgroundColor, style }) {
    const defaultStyle = {
        height: size,
        width: size,
        borderRadius: size / 2,
        backgroundColor: backgroundColor,
    }
    return(
        <Image style={[defaultStyle, style]} source={{uri: user.photo}}/>
    )

}
