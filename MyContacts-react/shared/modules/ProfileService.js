import {Settings} from "../Settings";
import {AsyncStorage} from "react-native";


const SettingsKeyName = 'MyContact:Settings';
export default class ProfileService{
    get(){
        return(AsyncStorage.getItem(SettingsKeyName).then((setting) => {
            console.log('ProfileService.get()=' + setting);
            return(new Settings(JSON.parse(setting)))
        }));
    }

    save(setting){
        const value = JSON.stringify(setting);
        return(AsyncStorage.mergeItem(SettingsKeyName, value)).then(() =>{
            return(setting);
        });

    }
}
